# Trouver des données dans un fichier structuré

>  Cette planche d'exercices repose sur le fichier Excel fournit par ailleurs.

## Exercice 1 - appréhender la structure de notre fichier

Le fichier fournit contient 3 onglets.
Parcourez ces trois fichiers pour comprendre ce que chacun représente.

- Expliquez sommairement ce que chaque onglet représente
- Expliquez le lien des données entre les différents onglets

> Prenez un peu de recul : ne décrivez pas le contenu de chaque colonne mais plutôt **pourquoi** nous avons voulu stocker ces données.
>
> Vous devrez notamment expliquer la différence entre l'onglet `Pokedex` et l'onglet `Pokemon`

## Exercice 2 - manipuler des données uniformes

Maintenant que l'on a un genre de vue d'ensemble de nos données, on va pouvoir commencer à se les approprier. Nous allons avancer onglet par onglet pour ne pas nous perdre :).

### Dans le premier onglet

- Combien de Pokemons sont de type feu :fire: ?
- Combien de Pokemons ne sont pas légendaires :star: ?
- Quels Pokemons ont une défense de base supérieure à 10 ?
- Quels Pokemons sont de type feu ET psy en même temps ?
- Quels Pokemons sont soit de type feu, soit de type combat (soit les deux) ?
- Quels Pokemons sont de type psy mais pas de type Chamalow ?
- Quel Pokemon NON légendaire a les meilleurs caractéristiques (on considère la somme de tous ses points, saufs les PV : Force + Intelligence + Vitesse + Défense) ?

### Dans le deuxième onglet

- Quels dresseurs, non maitres, ont plus de 1000€ ?
- Quel est le ratio des maitres Pokemons dans l'ensemble des dresseurs ?

### Dans le troisième onglet

- Combien de Pokémons n'ont pas de surnom ?
- Combien de Pokemons sont KO (n'ont plus de PV) ?
- Combien de Pokémons ont 4 attaques ?
- Combien de Pokémons ayant un surnom ne sont pas KO ?
- Combien de Pokémons n'ayant pas de surnom ne sont pas KO ?
- Quel est le niveau moyen de tous les Pokémons ?
- Quel est le niveau moyen de tous les Pokémons qui ne sont pas KO ?
- Celui de ceux qui sont KO ?

!!! note Réfléchissons un peu 🤔

    Maintenant que vous avez manipulé quelques données structurées, que pensez-vous de cette façon de stocker et de manipuler des données ? Vous pourriez creuser les axes suivants :

    - Et si j'avais mélangé mes trois types de données (Pokédex, Pokémons et Dresseurs) ?
    - Quels sont les avantages à structurer les données comme nous l'avons fait pour répondre aux questions ci-dessus ? Quels sont aussi les inconvénients que vous y voyiez ?
    - Pensez vous à une structuration qui ne soit pas celle-là et qui soit pratique pour répondre aux questions que l'on s'est posé dans cet exercice

## Exercice 3 - manipuler des données croisées

Maintenant que vous êtes à peu près à l'aise sur la manipulation de chaque type de données de manière indépendante, nous allons faire un pas en avant en essayant de les mettre en relation dans la mesure du possible. En effet, toutes ces données semblent tourner autour d'un même sujet et on peut imaginer qu'il existe des liens entre elles. Alors voyons cela maintenant !

L'onglet `Pokemons` est le pivot dans cet exercice et nous allons voir pourquoi :

- que représente la colonne `numéro du dresseur` ?
- que représente la colonne `numéro du pokemon` ?

Vous devriez donc maintenant mieux comprendre en quoi cet onglet est celui qui fait le lien avec les deux autres, sinon, il faut que vous copiez sur vos petits collègues !

> Cette étape est cruciale en base de données. Il faut être capable d'identifier les liens entre nos différents concepts, alors pratiquez cela sur des concepts de la vie de tous les jours si vous le pouvez :
>
> - les recettes de cuisine d'un restaurant, les ingrédients des plats et les commandes des clients
> - vos 20 films préférés, vos 20 acteurs préférés et l'information de qui a joué dans quel film
> - etc.

Maintenant que l'on est au clair avec cela, répondons à ces quelques questions :

- Combien de Pokemons possède Regix ?
- Quels dresseurs possèdent un pokémon légendaire ?
- Combien de Pokémons de type feu sont KO (n'ont plus de PV) ?
- De quelle ville est originaire le dresseur/la dresseuse qui possède le Pokémon de plus haut niveau ?
- Combien de Pokémons qui ne sont pas de type psy sont KO ?
- Combien de Pokémons ont toute leur vie (pv actuels = pv max) et appartiennent à des dresseurs possédant moins de 600 euro ?
- Combien de Pokémons de type eau possède Ondine ?

> Dans cet exercice, nous avons réalisé des liens entre 2 onglets, parfois entre 3 onglets. Dans l'absolu, il n'y a pas spécialement de limite au nombre d'onglets que l'on pourrait mettre en relation pour tirer une information :
>
> Du moment que l'on peut trouver un "lien" entre deux onglets au travers d'une colonne, c'est bon. On pourrait voir chaque onglet comme une ville et les colonnes qui font des liens comme des routes : amusez-vous à trouver des chemins vous permettant de visiter les villes !

## Exercice 4 - modifier certaines données

L'onglet `Pokemons` contient un problème... Un dresseur possède un Pokémon inconnu au bataillon ! Saurez vous débusquer le petit filou ?

Ajoutez ce Pokémon dans le Pokédex en lui donnant les caractéristiques que vous voulez, du moment que vous êtes aussi rigolos que le prof.

Par ailleurs, on veut aussi intégrer le support des Pokémons Shinny dans notre jeu : ces Pokémons sont comme les autres, exactement. N'importe quel Pokémon peut être shinny mais il est très rare de les rencontrer. Ce serait par exemple comme un animal albinos ou aux yeux veyron dans la vraie vie : il s'agira bien d'un hippocampe, mais avec un trait de caractère particulier et assez rare !

A quel endroit enregistreriez vous l'information selon laquelle un Pokémon est shinny ou non au vu de ces informations et pourquoi ?

## Conclusion

L'air de rien, nous avons ici manipulé les concepts fondamentaux d'une base de données. Cela étant, nous n'avons pas encore établi un vocabulaire formel, ni posé de définitions précises et cela ne va pas pouvoir continuer ainsi si on veut devenir solides :muscle:.