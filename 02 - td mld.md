# Modéliser nos données en partant d'un MLD

- [Modéliser nos données en partant d'un MLD](#modéliser-nos-données-en-partant-dun-mld)
  - [Exercice 1 - En partant d'un MLD](#exercice-1---en-partant-dun-mld)
    - [Etape 1 - S'approprier un MLD](#etape-1---sapproprier-un-mld)
    - [Etape 2 - Reproduire un MLD existant](#etape-2---reproduire-un-mld-existant)
    - [Etape 3 - Puis le faire évoluer](#etape-3---puis-le-faire-évoluer)
  - [Exercice 2 - En partant de l'expression d'un besoin](#exercice-2---en-partant-de-lexpression-dun-besoin)
    - [Etape 1 - S'approprier la demande](#etape-1---sapproprier-la-demande)
    - [Etape 2 - Un premier contact avec SQL ( 0..0)](#etape-2---un-premier-contact-avec-sql--00)
    - [Etape bonus - Instancier sa base de données](#etape-bonus---instancier-sa-base-de-données)

Lors de notre séance précédente nous avons appréhendé le concept de base de données : nous avons vu qu'il nous fallait structurer nos données de façon cohérente, en identifiant tous les "concepts" que notre application doit manipuler. 

> Pour rappel, un onglet de notre classeur Excel correspondait à un "concept".

Nous avions également vu qu'il existait des liens entre nos différents onglets au travers de certaines colonnes. Nous avons même vu que ceci avait un nom en bases de données : les `clés étrangères` !

Et bien figurez-vous que lorqsue l'on représente nos concepts ET nos clés étrangères sous forme d'un graphique avant de nous lancer dans la construction d'une base de données, on bénéficie de :

- une représentation assez visuelle et facile à manipuler pour trouver la meilleure organisation possible (phase de création/conception)
- une description suffisamment détaillée pour pouvoir implémenter notre base une fois que l'on est satisfait

Et comme les choses sont bien faites, on a décidé de donner un nom à ce type de graphiques : les `Modèles Logiques de Données`, abrégé en *MLD*.

> Vous pourrez parfois entendre parler de `modèle relationnel`, c'est la même chose. C'est même plus parlant puisque chaque élément que l'on représente dans ce graphique est appellé une `relation`.

Le MLD est donc un bon outil pour prendre un peu de recul sur les données que l'on doit manipuler. Il nous permet d'un peu conceptualiser tout ça avant de nous lancer. Sur une toute petite base, on pense souvent pouvoir s'en passer mais dès que la complexité augmente un peu, c'est le type d'outil qui nous parait vite indispensable.

On appelle une `relation` tout concept que l'on représente dans le MLD (un onglet dans notre fichier Excel <=> une relation).

!!! hint Pour aller plus loin

    Il existe un degré d'abstration (une prise de recul supplémentaire) nous permettant de réfléchir à des structures encore plus complexes et d'apporter plus de rigueur et de sens à nos données. Il s'agit du `Modèle Conceptuel de Données` (ou MCD, ou encore Modèle Entité-Association). Vui vui. Nous verrons cela car c'est un outil très puissant mais partir du plus concret pour prendre de la hauteur pourrait vous aider à sentir l'intêrét de cette approche.

Dans cette séance, nous allons donc nous approprier cet outil qu'est le MLD !

!!! example A vous de jouer (1h30)

    Le MLD comme le MCD sont des outils décrits dans la méthode Merise (que l'on va un peu approfondir dans les séances à venir). Cette méthode de modélisation de bases de données assez complète et puissante est essentiellement française. De fait, lorsque l'on parle de "modéliser une base de données" en France, cela signifie généralement déployer une méthode Merise alors que dans la majorité des pays, cela signifie produire un MLD. Nous nous satisferons tout à fait de cela pour aujourd'hui.

    Il existe des programmes (en ligne ou à télécharger) permettant de modéliser des bases de données au travers de MLD. Trouvez-en 3 et expliquez un peu comment ils fonctionnent (captures d'écran, fonctionnalités, avantages/inconvénients au premier abord, celui que vous préférez, etc.) et faites en une petite synthèse dans un document Word ou Markdown.

    > Si vous ne trouvez pas de logiciel, focalisez-vous sur **Quick Database Diagram* et expliquez de la façon la plus détaillée possible comment il fonctionne

## Exercice 1 - En partant d'un MLD

Nous allons partir du `modèle relationnel` représentant la structure de données d'une école. Nous allons bien évidemment simplifier en nous focalisant sur certains aspects au détriments d'autres. Cela pourrait donner quelque chose comme :

![](./02-mld-cours.jpg)

### Etape 1 - S'approprier un MLD

- Expliquez ce que représentent selon vous les `relations` du MLD (1 - 3 phrases par relation)
- Identifiez les `clés primaires` de chaque `relation`
- Identifiez et expliquez le sens de toutes les `clés étrangères`

### Etape 2 - Reproduire un MLD existant

- dans le logiciel de modélisation de bases de données de votre choix, reproduisez ce schéma (un vrai bon plagiat, histoire d'apprendre à manipuler ces outils). Vous ferez bien attention à respecter :
    - les noms des `relations`
    - les `clés primaires` (ou PK pour *private key*)
    - les `clés étrangères` (ou FK pour *foreign key*)
    - le types de chaque `propriété` (chaque ligne qui se trouve dans une relation)

### Etape 3 - Puis le faire évoluer

- ajouter une relation `filieres` qui permettra de représenter toutes les filières qu'une école propose et faites un lien avec les classes : une classe est rattachée à une filière
- modifier la relation `intervenants` parce que l'on en distingue deux types : on distingue donc les `employés` qui ont un **type de contrat** (CDD ou CDI) des `prestataires` qui ont juste (ou non) un **contrat actif**. Cette question soulève pas mal de problèmes si on souhaite supprimer la relation `intervenants` pour en fabriquer deux nouvelles mais il existe évidemment de bonnes solutions que nous verrons ensemble ! Tentez quelque chose :).

> A ce stade, vous commencez à comprendre ce qu'est un MLD. Vous allez donc pouvoir voir comment cet outil est utilisé dans un vrai projet.

## Exercice 2 - En partant de l'expression d'un besoin

Dans cet exercice, on va faire l'exercice inverse, qui ressemble plus à ce que l'on rencontre dans la vraie vie : on part d'un besoin informel exprimé par un client et on doit structurer et formaliser tout ça.


!!! note Description de nos données

    Un zoo souhaite mettre en place une meilleure gestion de son parc en créant une application permettant de mieux positionner ses employés dans les différents secteurs et de mieux disposer les animaux pour qu'ils soient plus épanouis et pour amener plus de cohérence pour les visiteurs.

    Le zoo est composé de plusieurs secteurs qui ont tous un nom, un numéro et qui contiennent chacun plusieurs enclots. Par ailleurs un secteur regroupe une `famille taxonomique` unique d'animaux (genre `Leporidae` pour les lapins, `Equide` pour les chevaux ou les zèbres, etc.). 
    
    Un enclot contient 0, 1 ou plusieurs animaux d'une même espèce (sinon on peut avoir des soucis !). Un enclôt a une superficie, une température moyenne à respecter, un taux d'humidité moyen et une durée d'ensoleillement moyen.

    On souhaite avoir une vision de la classification des animaux : pour chaque espèce, on souhaite connaitre sa famille taxonomique ainsi que son [statut de conservation](https://fr.wikipedia.org/wiki/Statut_de_conservation). 

    Les animaux sont caractérisés par leur nom, leur espèce, leur date de naissance et leur poids et l'enclot dans lequel ils se trouvent.

    Enfin, du côté du personnel, on identifie esentiellement deux profils
    - Les soignants qui sont spécialisés dans une famille d'animaux et le zoo doit avoir au moins un soignant par famille d'animaux représentée.
    - Les animateurs qui sont responsables d'un ou plusieurs enclots, sans contrainte particulière sur le secteur (ils peuvent tous être dans le même secteur ou non)



### Etape 1 - S'approprier la demande

!!! warning Avant de nous envoyer !

    Dans cet exercice nous allons produire un MLD dans un logiciel de votre choix **MAIS** ce logiciel doit offrir une fonctionnalité que nous utiliserons dans l'étape 2, avant de vous lancer, demandez-moi si votre logiciel le permet ou pas, pour que vous évitiez de perdre du temps... :)

- Identifiez de cette définition du besoin l'ensemble des relations : tous les concepts que l'on va devoir stocker en base pour couvrir la demande.
- Identifiez tous les liens entre les relations, qui vont donc prendre la forme de clés étrangères
- Identifiez les `propriétés` de chaque `relation`
- Une fois que vous avez tout cela, modélisez ceci dans le logiciel de modélisation de bases de données de votre choix (du moment qu'il permet de faire un MLD hein...)

Justifiez vos choix en quelques lignes : 

- avec les relations que vous avez retenues, couvrez vous l'ensemble des données que l'on souhaite modéliser ?
- pour chaque clé étrangère, expliquez à quel "morceau" du texte elles correspond
- toute autre explication que vous jugeriez utile !

### Etape 2 - Un premier contact avec SQL ( 0..0)

On y est. Le boss final du jour : SQL !

Vous allez devoir trouver comment `exporter` votre schéma au format SQL, si possible en PostgreSQL d'ailleurs.

Une fois cela fait, vous devriez obtenir un fichier dont l'extension est `.sql`. Ouvrez donc ce fichier avec votre éditeur de texte préféré et essayez d'expliquer un peu la correspondance entre ce code et votre MLD produit à l'étape précédente.

### Etape bonus - Instancier sa base de données

Si vous êtes ici, c'est que vous avez un peu plus de courage que la moyenne ! Alors déjà bravo :). Ensuite, nous allons nous attaquer à une étape assez sympa qu'est le déploiement effectif de notre base de données !

- La première étape va donc être d'installer Postgres sur votre ordinateur
- Une fois cela fait, vous devriez avoir un nouveau logiciel nommé `PGadmin4`, lancez le :rocket: (c'est un *lance requête SQL*... Pardon...)
- Importe-y le fichier SQL généré à l'étape précédente, soit en suivant votre instinct, soit en suivant [ce tutoriel en anglais](https://www.pgadmin.org/docs/pgadmin4/development/import_export_data.html)

Une fois cela fait, votre base pourrait bien exister et être déjà disponible, n'attendant plus qu'une petite application pour l'utiliser ! 

> Il se peut qu'avant d'importer ce fichier, vous deviez créer une base de données vide, ce que vous pouvez faire directement dans l'interface de PGadmin :).

Nous sommes ici en fin de chaine de ce que nous verrons dans ce cours. A vrai dire, nous avons même déjà mis un pied dans le cours de programmation SQL que vous aurez prochainement. Il s'agissait donc ici d'une étape bonus, pas si simple que ça à mettre en place sans plus d'information. Mais pas de panique, vous aurez toutes les informations pour comprendre bien cette étape dans le module de programmation base de données !

Pour notre part, nous allons reprendre la route dans l'autre sens et consolider nos compétences en modélisation/conception afin que vous puissiez avoir tout le socle de connaissances nécessaire à la maitrise absolue et inconditionnelle des bases de données.
