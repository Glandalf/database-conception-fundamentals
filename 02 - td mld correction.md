# Correction du TD #02

- [Correction du TD #02](#correction-du-td-02)
  - [Exercice 1 - En partant d'un MLD](#exercice-1---en-partant-dun-mld)
    - [Etape 1 - S'approprier un MLD existant](#etape-1---sapproprier-un-mld-existant)
      - [Question 1](#question-1)
      - [Question 2](#question-2)
      - [Question 3](#question-3)
    - [Etape 2 - Reproduire un MLD existant](#etape-2---reproduire-un-mld-existant)
    - [Etape 3 - Puis le fait évoluer](#etape-3---puis-le-fait-évoluer)
      - [Question 1](#question-1-1)
      - [Question 2](#question-2-1)
  - [Exercice 2 - En partant de l'expression d'un besoin](#exercice-2---en-partant-de-lexpression-dun-besoin)
    - [Etape 1 - S'approprier la demande](#etape-1---sapproprier-la-demande)
    - [Etape 2 - Un premier contact avec SQL ( 0..0)](#etape-2---un-premier-contact-avec-sql--00)
    - [Etape bonus - Instancier sa base de données](#etape-bonus---instancier-sa-base-de-données)
      - [Question 1](#question-1-2)
      - [Question 2](#question-2-2)

## Exercice 1 - En partant d'un MLD

### Etape 1 - S'approprier un MLD existant

#### Question 1

- `salles` L'ensemble des salles qui peuvent accueillir des cours. Elle associe un numéro à une position géographique.
- `cours` Un créneau d'une durée donnée pour un cours, dans une salle avec une classe et un intervenant.
- `intervenants` L'ensemble des intervenants de l'école.
- `etudiants` L'ensemble des personnes inscrites dans l'école. Un étudiant est rattaché à une classe, ce qui veut dire que chaque année, il faut le faire passer dans la classe supérieure !
- `classes` Une classe est un ensemble d'étudiants. Il y a chaque année autant de nouvelles classes que l'on a de promotions/filières. Par exemple, il y aura une classe `switch IT B1` en 2020-2021, puis une autre en 2021-2022, etc.
- `personnelAdministratif` Chaque membre de l'administration peut gérer une ou plusieurs classe. Une classe DOIT être gérée par un membre de l'administration

> Remarque : le MLD proposé a un souci de type pour le champ (ou la propriété) `intervenants.tel` qui devrait être limité à 14 caractères si l'on s'en tient à ce que l'on a défini dans les autres relations.

#### Question 2

Liste des clés primaires :

- `salles` =>  numero (numéro de la salle, il s'agit d'un `identifiant fonctionnel`)
- `cours` => id (`identifiant technique`, qui n'a pas de sens ni d'intérêt pour les utilisateur mais qui nous permet d'identifier un cours de manière unique)  
- `intervenants` => id (pareil)  on aurait pu utiliser un numéro de sécurité sociale comme `identifiant fonctionnel` à la place.
- `etudiants` => id (pareil) on aurait pu utiliser un numéro de sécurité sociale comme `identifiant fonctionnel` à la place.
- `classes` => id (pareil MAIS on aurait pu avoir un identifiant fonctionnel en utilisant le nom de la classe et en le combinant à l'année scolaire en cours. Si on fait cela, notre clé primaire est composée de deux champs, on parle de `clé primaire composite`) 
- `personnelAdministratif` => id (pareil que tout sauf classe) 

#### Question 3

- cours >- salles : un cours se passe dans une salle, une salle peut accueillir plusieurs cours
- cours >- intervenants : Un intervenant peut assurer un ou plusieurs cours, un cours sera assuré par un seul intervenant
- cours >- classes : un cours se déroule pour une seule classe et une même classe à plusieurs cours
- etudiants >- classes : un étudiant appartient à une classe, une classe comporte plusieurs étudiants. D'une année sur l'autre, la classe change, on doit donc mettre à jour ce lien chaque année.
- classes >- personnelAdministratif : une classe est sous la responsabilité d'un seul membre du personnel, un membre du personnel peut avoir 0, 1 ou plusieurs classes sous sa responsabilité (on pense à un responsable pédagogique par filière par exemple)

### Etape 2 - Reproduire un MLD existant

![](./02-mld-depart-cocrrection.png)
> MLD de départ



### Etape 3 - Puis le fait évoluer

#### Question 1

> cf. la relation filieres créée dans le MLD ci-après. On a expliqué que l'on crée une contrainte du côté de l'association qui "contient la pluralité" : le côté `many` de l'association `one to many` est celui qui décrit le champ faisant référence au côté `one`

#### Question 2

> On fait ici intervenir la notion d'héritage et plus particulièrement de spécialisation. On a donc une relation parente/mère (intervenants) et des relations enfants/filles (presta et employés). On a vu deux façons de créer les clés étrangères, donc une qui est mauvaise : on les décrit toujours dans les relations filles.

![](./02-mld-corrige.png)




## Exercice 2 - En partant de l'expression d'un besoin

### Etape 1 - S'approprier la demande

> Ces questions là on été traitées très rapidement et de façon informelle comme note de notre futur MLD 

![](02-mld-zoo.png)

Il aurait VRAIMENT fallu venir justifier tous nos choix, par exemple en répondant formellement aux 3 questions, car un MLD tout seul, c'est une mauvaise pratique.


### Etape 2 - Un premier contact avec SQL ( 0..0)

Le logiciel utilisé durant la correction ne permet qu'un export table par table en version gratuite. 
Nous en exportons donc deux pour information grâce au bouton de *forward engineering* sur la gauche de l'interface de https://app.sqldbm.com une fois que nous avons fini notre MLD et voilà le résultat pour nos deux tables :

```sql
CREATE TABLE enclosAnimateurs
(
 animateur int NOT NULL,
 enclos    int NOT NULL,
 CONSTRAINT PK_enclosanimateurs PRIMARY KEY ( animateur, enclos ),
 CONSTRAINT FK_406 FOREIGN KEY ( enclos ) REFERENCES enclos ( "id" ),
 CONSTRAINT FK_413 FOREIGN KEY ( animateur ) REFERENCES animateurs ( "id" )
);

CREATE INDEX fkIdx_407 ON enclosAnimateurs
(
 enclos
);

CREATE INDEX fkIdx_414 ON enclosAnimateurs
(
 animateur
);
```

> Script de création de notre table d'association

```sql
CREATE TABLE especes
(
 "id"                 int NOT NULL,
 statutConservation int NOT NULL,
 familles           int NOT NULL,
 CONSTRAINT PK_especes PRIMARY KEY ( "id" ),
 CONSTRAINT FK_381 FOREIGN KEY ( statutConservation ) REFERENCES statutsConservation ( "id" ),
 CONSTRAINT FK_384 FOREIGN KEY ( familles ) REFERENCES familles ( "id" )
);

CREATE INDEX fkIdx_382 ON especes
(
 statutConservation
);

CREATE INDEX fkIdx_385 ON especes
(
 familles
);
```

> Script de création de la table especes

### Etape bonus - Instancier sa base de données

#### Question 1

#### Question 2
