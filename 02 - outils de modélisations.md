# Outils de modélisation de Bases de Données

Nous avons vu qu'une base de données peut être modélisée sous forme d'un diagramme que l'on appelle un `MLD`. Nous avons aussi évoqué qu'il existe un autre type de graphiques, plus riche mais aussi plus complexe à appréhender, que l'on appelle `MCD`.

## Les outils de modélisation de MCD

Nous ne traitons pas cette étape pour le moment. Les logiciels de modélisation de base de données partant d'un MCD sont assez rares, puisqu'essentiellement utilisés en France. L'offre en termes d'outils est donc hélas relativement limitée.

> TODO: agrémenter cette partie lorsque l'on aura vu ce qu'est un MCD !

### Looping

> Voir Looping dans la partie MLD

### Mocodo

URL: https://mocodo.net/

Disponible en ligne ou en client lourd. Approche "code" : on écrit au format texte selon leur formalisme et le logiciel génère le graphique (MCD) mais aussi d'autres ressources : MLD, MPD, représentation graphique (format image) de ces éléments là, et même un document expliquant notre modèle relationnel.

Peut-être moins "swaggy" que QuickDBD mais dans la même veine, avec plus de possibilités d'export.

### DB main

URL:

Client lourd, qui permet la modélisation du MCD, puis la dérivation en MLD, puis la dérivation en SQL. Cela semble un bon outil, visiblement payant 

## Les outils de modélisation de MLD

### QuickDBD

URL :

100% en ligne et gratuit. Conception format texte avec rendu graphique

### SQLDBM

URL: https://app.sqldbm.com/

100% en ligne et gratuit. Assez joli, conception 100% graphique

### lucid.app

URL: https://lucid.app/lucidchart

C'est un outil en ligne sans avoir à installer. Approche freemium : gratuit sur les fonctionnalités simples, puis abonnement pour débloquer les fonctionnalités avancées.
Pas spécifiquement orienté MLD.

### diagrams.net (ou draw.io)

URL: https://app.diagrams.net

Déjà utilisé en cours pour faire des diagrammes de classes.
Similaire à lucidchart, mais totalement gratuit et donc avec plus de fonctionnalités de base. Possibilité de travail collaboratif probablement (à creuser).
Possibilité d'utiliser plusieurs onglets dans un même fichier (façon Excel).

### Looping

URL: looping-mcd.fr

Permet de faire du MCD, MLD et UML ! Totalement gratuit, client lourd sans installation. Un peu moins joli que d'autres mais très complet.

