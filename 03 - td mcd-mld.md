# TP à rendre

!!! note Consignes de rendu :)

    Ce document est le sujet d'un travail noté, que vous devez rendre d'ici le vendredi 12 février, par mail à renaud.angles@campus.academy :

    - un mail par groupe pour l'exercice 1 (mettez vos collègues en copie ci possible)
    - un mail par personne pour l'exercice 2

    Ce travail est composé de deux exercices, qui produiront deux notes (la première de groupe, l'autre individuelle).

    - Le premier exercice doit donc être fait en groupe de 2 à 5 personnes maximum (désolé, pas de possibilité d'outrepasser ce prérequis :(). Le rendu est :
      - un document écrit commun traitant chaque point et sous point demandé dans l'exercice
      - une capture d'écran (ou autre format pratique à l'usage) de votre MLD final. Les deux questions de la partie "Faire évoluer" nécessitent une évolution du MLD donc, mais aussi une explication écrite de ce en quoi vous résolvez le problème 
    - Le deuxième exercice est donc individuel et consiste essentiellement en la production d'un MLD, mais n'oubliez pas pour autant de prendre des notes et justifier vos choix. Le rendu est donc :
      - un document écrit justifiant tous vos choix et répondant à chaque question
      - un MLD (que vous pouvez intégrer directement dans le document si vous le souhaitez)

    N'hésitez pas à poser des questions si certains points ne vous semblent pas clairs ou pour vous rassurer :rocket:. Bon courage !

## Exercice 1 - Interpréter un modèle de données (travail de groupe)

Un "modèle de données" peut aussi bien faire référence à un MCD, un MLD ou MPD. Vous en avez déjà analysé un lors de la scéance précédente et c'est ce que l'on va refaire ici, en allant vers un peu plus de rigueur.

![aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa](03-mld-voitures.png)

### Analyser

- Expliquez en une ou deux phrases ce que représente chaque relation du modèle ci-dessus
- Indentifiez toutes les `clés primaires composites`
- Identifiez pour chaque **lien** s'il s'agit d'une assocation `1 to 1`, `1 to many` ou `many to many`
- Ayant posé au clair quelques éléments de notre modèle, vous pouvez maintenant en faire une explication globale du modèle
Expliquez notamment mais pas seulement :
    - quelle est l'utilité d'avoir une relation InformationsEntreprises ?
    - que représente le lien entre `concessionnaire` et `Constructeur` ?
    - pourquoi la relation `PiecesModeles` n'a pas de champs autre que la clé primaire ?
    - pourquoi la relation `ModelesConcessionnaires` a des champs autre que la clé primaire (en comparaison avec la question précédente) ?

### Faire évoluer

> Une étape préliminaire à la réalisation de cette partie de l'exercice est de reproduire ce MLD dans le logiciel de votre choix (référez vous à [notre prise de notes sur les outils de modélisation](https://gitlab.com/Glandalf/database-conception-fundamentals/-/blob/master/02%20-%20outils%20de%20mod%C3%A9lisations.md)) pour choisir un outil par exemple.

- Actuellement, les constructeurs n'ont pas une bonne vision des options de leurs voitures. Ils sont obligés de créer les mêmes options (comme la `navigation GPS` pour chaque modèle) en raison des limitations actuelles de notre modèle. Un des soucis de cette limitations est qu'il existe plusieurs noms différents représentants une même option (`gps`, `GPS`, `navigation`, etc.). Cela rend très compliqué la production de statistiques et difficile l'évolution des prix par exemple. Faites en sorte de résoudre ce souci en proposant une relation *many to many* à la place.
- **bonus :** Les concessionnaires ont un autre souci : ils n'arrivent pas à garder trace de l'évolution du prix des voitures qu'ils vendent : s'ils changent le prix dans `ModelesConcessionnaires`, ils perdent l'ancien prix. Hors ils ont besoin d'avoir une vision de ces changements. Il faut donc que vous créiez une relation qui historisera les prix : "de telle date à telle date, tel modèle coutait tant".

## Exercice 2 - Produire un modèle de données (travail individuel)

!!! example La demande

    Nous souhaitons créer une base de données pour un moteur de recherche musical permettant de rechercher des titres, interprètes, plateformes d'écoute, etc.. Il s'agit donc d'une approche assez large, touchant à de nombreux métiers/entités ayant trait à la musique. On souhaite que notre base respecte les règles suivantes :

    - une personne est au moins auteur, compositeur, interprète ou producteur (elle peut exercer plusieurs de ces métier) et est caractérisée par un nom, un prénom, une nationalité et une date de naissance
    - un titre a **au moins** un auteur, **au moins** un interprète, un style musical, un compositeur, un nom, une durée et peut être rattaché à un album
    - un album a un producteur, une date de sortie, un titre, un prix et au moins un titre (au sens d'une musique ici)
    - un auteur écrit dans une seule langue
    - un compositeur compose pour un ou plusieurs styles musicaux
    - une plateforme d'écoute peut être de type streaming ou vente (pas les deux), est rattachée à un producteur et a un nom et un site web
    - un album **peut** être diffusé sur une ou plusieurs plateformes d'écoute

    Une liste des styles musicaux nous sera fournie le jour où l'on implémentera la base de données, il faudra donc pouvoir s'assurer de les intégrer et que les différents éléments de notre modèle ne puisse faire référence qu'à des styles de cette liste.


### S'approprier la demande

Comme nous l'avons déjà fait jusque là, nous allons commencer par nous approprier la demande :

- Identifiez de cette définition du besoin l'ensemble des relations : tous les concepts que l'on va devoir stocker en base pour couvrir la demande.
- Identifiez tous les liens entre les relations, qui vont donc prendre la forme de clés étrangères et essayez de voir s'il s'agit d'associations `1 to 1`, `1 to many` ou `many to many`
- Identifiez les `propriétés` de chaque `relation`
- Une fois que vous avez tout cela, modélisez ceci dans le logiciel de modélisation de bases de données de votre choix

### Bonus : monter une base de données

Une fois votre modèle fini, vous devriez pouvoir l'exporter au format SQL de votre choix, par exemple Postgres ou MySQL/MariaDB. Vous obtiendrez alors un fichier SQL contenant toutes les requêtes permettant la création d'une base de données. Joignez ce fichier SQL à votre travail de rendu

Installez (si vous ne l'avez pas déjà) le SGBD correspondant (Postgres, etc.) et importez y le fichier sql. Le résultat que vous devriez obtenir est une nouvelle base de données créée, avec toutes les tables que vous aviez modélisé. Prenez une ou plusieurs captures d'écran de l'interface d'administration (PgAdmin ou PHPMyAdmin) qui montre ce résultat.

Si vous êtes aventurier, vous pourriez même vous risquer à créer quelques auteurs ou autre dans votre base de données, depuis l'interface d'administration ou directement en lignes de commandes même !
Ajoutez, là encore quelques captures d'écran pour illustrer vos tentatives.
