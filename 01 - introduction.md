# Modélisation des bases de données

Plutôt que partir de définitions très théoriques, partons d'une question :

!!! question Comment stockeriez-vous des données pour vos applications ou sites ?

    - Physiquement, faut-il qu'elles soient stockées en RAM ou sur disque dur ?
    - Du coup, vous les stockeriez dans quoi : des variables, des fichiers, autre ?
    - Comment y accèderiez vous par la suite quand vous en auriez besoin ?
    - Il existe 4 grandes opérations que l'on réalise généralement sur des données :
      - en écrire de nouvelles
      - en modifier des existantes
      - en supprimer
      - en rechercher (pour les lire)

      Une fois cela posé, comment pourrait-on structurer nos données pour qu'elles permettent ces 4 types d'opération ?

- stockage sur disque dur, cloud, serveur
  - si on stocke en RAM, lorsque l'on éteint l'ordinateur on perd les informations.
  - si on les stocke sur disque, on n'a pas ce souci : on parle de pérénité des données
  - un "cloud", c'est un serveur, auquel j'ai accès d'une façon assez spécifique, au travers d'applications et de services divers. Et dans tous les cas, c'est bien sur disque qu'on va stocker nos données
  - si le disque dur tombe en panne : on perd les données. Pour éviter cela, on met en place des mécaniques de réplication et de sauvegarde des données.
    - on peut par exemple mettre en place un RAID logiciel ou matériel (on écrit les mêmes données sur plusieurs disques en faisant comme si on en n'avait qu'un)
    - on peut sauvegarder régulièrement nos données en copiant tous les fichiers que l'on souhaite vers un autre disque
    - on peut même sauvegarder tout notre serveur (ghost pour un serveur physique ou snapshot/point de contrôle dans le cas d'une VM)
  > Si on stocke sur un cloud/serveur, on considère que c'est le service que l'on utilise qui assure cette sécurité, mais on pourrait tout aussi bien la mettre en place sur nos propres ordinateurs.
- données rapide d'accès
  - il est compliqué de stocker des données sur SSD et M2 pour des raisons de volumétrie : une base de données peut assez rapidement dépasser quelques To de données, ce qui couterait extrêmement cher sur des disque à mémoire flash. On stocker donc généralement cela sur des **disque durs** classiques qui sont assez lents (beaucoup plus que leur cousins en flash ou que la RAM).
  > Soit je stocke beaucoup de données, soit je peux y accéder rapidement. On accepte généralement de perdre un peu de vitesse pour être sûr que l'on pourra stocker toutes nos données à un prix raisonnable.
- accéssibilité des données (pour nous qui écrivons des programmes)
  - Ce qu'on préfère, nous développeurs fainéants, c'est utiliser des variables. Mais celles-ci sont stockées dans la RAM. On aimerait bien un moyen d'accéder assez facilement à nos données, tout en sachant qu'elles sont enregistrées sur disque dur. Notons que lorsque l'on écrit des choses sur disque c'est forcément sous forme de fichier...
  > C'est justement l'un des objectifs d'une base de données : faire la passerelle entre les fichiers difficiles à manipuler et notre code que l'on veut "facile" à écrire.

Lorsque l'on veut stocker nos données dans un fichier, il faut choisir un format qui va être pertinent. Prenons l'exemple d'un post Instagram (photo + message)

- On pourrait stocker les photos dans un dossier dédié : un post <=> un fichier dans mon dossier. Restons en là pour les photos.
- Pour le message, on pourrait stocker cela dans un docx, sous forme de liste à puce. **Avec nos listes à puce, on amène de la structuration**.
  > Inconvénient : Word est un peu lent et pas très simple à manipuler depuis un programme. Du point de vue de mon programme qui va lire et écrire mon fichier, les fonctionnalités de Word ne sont pas nécessaire (mise en forme, jolie interface, etc.).
- On peut essayer de trouver une alternative similaire mais plus rudimentaire (et donc plus simple à manipuler). Les fichiers texte, toujours sous forme de liste à puce, comme en Markdown par exemple pourraient faire l'affaire : plus facile à manipuler du point de vue de la programmation, c'est moins cher et on pourrait trouver d'autres avantages peut-être.
  > Inconvénient : si je veux stocker le message du post mais aussi l'auteur, et la date, comment je fais ?

### Structure des données

On pourait conserver notre idée de liste et ajouter un caractère particulier pour dire que l'on passe à l'information suivante :

```
- Mon premier post#Renaud ANGLES#10/12/2018
- Vacances au ski#Jean JEAN#11/08/2020
- Ptit dej OKLM#Jean JEAN#12/08/2020
```

> Une ligne = 1 post, le `#` permet de séparer le message, de son auteur, puis de la date. On pourrait utiliser n'importe quel caractère de notre choix

Des gens ont été d'accord avec votre idée et l'on un peu épuré.
On peut probablement se passer du `- ` au début de chaque ligne puisque une ligne = 1 post. Et ainsi naquit le format [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values). CSV utilise les virules comme séparateurs alors que l'on a utilise le dièse mais c'est pareil.

```csv
Mon premier post,Renaud ANGLES,10/12/2018
Vacances au ski,Jean JEAN,11/08/2020
Ptit dej OKLM,Jean JEAN,12/08/2020
```

Ce format commence à bien nous plaire puisqu'il nous permet de stocker des données un peu plus complexes, ultra rapides d'accès et de traitement par un programme. C'est le type de format idéal pour stocker et manipuler des données.

Il existe tout de même d'autres approches qui souhaitent privilégier un peu de lisibilité pour l'utilisateur mais aussi et surtout, qui permettent de stocker des informations plus complexes. Dans notre exemple de posts, on a seulement trois informations, "linéaires", sans "sous-informations". En programmation nous utilisons des types comme `int`, `float`, `char`, `string`. Il existe plein d'autres types, pour certains aussi "simples", pour d'autres un peu plus complexes, qui prennent parfois la forme de types, parfois la forme de structures. On citera l'exemple le plus répendu : les tableaux ou `array`. Dans le cas d'un post Instagram, on peut vouloir stocker un tableau de commentaires ou de réactions (émojis) par exemple.

Le format CSV n'est pas très adapté à ce genre de problème même si on connait de très bonnes manières de gérer cela sur des formats de type CSV. Cela étant, on aimerait parfois structurer cela de manière un peu plus précise. `JSON` et `XML` sont de très bons formats pour stocker des informations de cette manière.

```json
[
    {
        "utilisateur": "Renaud ANGLES",
        "message": "Mon premimer post",
        "date": "10/12/2018"
    },
    {
        "utilisateur": "Jean JEAN",
        "message": "Vacances au ski",
        "date": "11/08/2020"
    }
]
```

> `tableau` de nos posts, sous forme d'`objet` JSON

```xml
<posts>
    <post>
        <utilisateur>Renaud ANGLES</utilisateur>
        <message>Mon premier post</message>
        <date>10/12/2018</date>
    </post>
    <post>
        <utilisateur>Jean JEAN</utilisateur>
        <message>Vacances au ski</message>
        <date>11/08/2020</date>
        <commentaires>
            <commentaire>
                <utilisateur>Renaud ANGLES</utilisateur>
                <message>C'est la bombe</message>
                <date>11/08/2020</date>
            </commentaire>
        </commentaires>
    </post>
</posts>
```

> Même chose en XML, sous forme de `balises` donc

!!! note Conclusion

    Nous avons petit à petit déterminé que des formats comme CSV, JSON ou XML sont de très bons candidats à la structuration de nos données.
    Cela tombe bien, pratiquement tous les langages proposent de nombreux outils pour manipuler les fichiers de cette nature.

    - Les formats comme CSV sont dit `matriciels` : c'est un tableau avec des lignes et colonnes quoi... Et ils sont beaucoup utilisés dans les bases de données traditionnelles.
    - Les formats comme XML et JSON sont dits `arborescents` : c'est des "objets" qui peuvent contenir des objets, contenant à leur tour...Ce type de fichiers est parfois utilisé dans les bases de données NoSQL.

## Ce que vous connaissez déjà du monde des BDD

!!! note Remarque

    Il est possible que vous ne connaissiez encore rien dans ce domaine et ce serait tout à fait normal. Mais si certains d'entre vous ont entendu parlé (ou manipulé) certains concepts, ce sera l'occasion d'un échange et d'une tentative de mise à plat de tout cela.

- `SQL`, c'est un langage de manipulation de bases de données
- `MySQL`, c'est un outil de gestion de bases de données (`SGBDr`, Système de Gestion de Bases de Données relationnelles) qui utilise le langage SQL de manière assez efficace : il est assez rapide selon le moteur qu'il utilise (vous verrez cela plus tard) mais il est moins sécurisé quand à la qualité de vos données.
- `MariaDB`, c'est ausssi un SGBD. MySQL a été inventé par l'entreprise SUN Microsystem. Mais un jour Oracle rachête SUN (et donc MySQL). La communauté Open Source autour de MySQL a eu un peu peur et a décidé de faire un "clone" (un `fork` du code de MySQL pour assurer qu'il reste gratuit, libre et open source). C'est donc de là qu'est né MariaDB. MySQL est finalement resté assez libre, les deux SGBD sont donc toujours très similaire malgré la séparation il y a longtemps.
- `Phpmyadmin`, c'est une interface graphique web pour MySQL et MariaDB (on parle d'un `client MySQL` ou `client MariaDB`). On peut l'utiliser pour :
  - créer une BDD pour Wordpress par exemple
  - lister toutes les bases de données que l'on possède sur notre SGBD
  - insérer des données, les modifier, les supprimer et le rechercher
  - gérer les utilisateurs et leurs droits d'accès
  - poermet beaucoup d'autres choses que vous découvrirez par vous même :)
  
  Il existe de nombreux autres clients MySQL, avec interface graphique ou non, gratuits ou non, etc.
- `tables` : une table en BDD c'est une manière de représenter les données sous forme tabulaire ou `matricielles` : c'est des lignes et des colonnes qui permettent de stocker des informations "homogènes". Une table contient un type de données et je peux avoir plusieurs tables (une table `posts` par exemple, une table `commentaires` et une table `utilisateurs`)
- `MongoDB` est un SGBD non relationnel. C'est une technologie NoSQL contrairement à MySQL ou MariaDB par exemple. Là où tous les SGBDr (utilisant SQL donc) sont très similaires, les SGBD NoSQL peuvent avoir des fonctionnement très variés, parfois identique à SQL, parfois pas du tout : c'est la jungle !

## Un premier contact avec les bases de données

!!! hint Partons d'un exemple concret

    Nous allons commencer par ouvrir [ce fichier](https://teams.microsoft.com/l/file/525E9DCE-E7FD-4AEF-BCFD-F7DB3F9DEDCD?tenantId=33477cd1-5a09-4690-bbb3-d0ffa0b519ad&fileType=xlsx&objectUrl=https%3A%2F%2Fimie365.sharepoint.com%2Fsites%2FCAPdaAixswitchITB1%2FDocuments%20partages%2FGeneral%2FDB%20Conception%20Fundamentals%2F01%20-%20%20exercices.xlsx&baseUrl=https%3A%2F%2Fimie365.sharepoint.com%2Fsites%2FCAPdaAixswitchITB1&serviceName=teams&threadId=19:0bc6c2dade0645518ca7fb21521b039c@thread.tacv2&groupId=dc14a71f-94da-4239-b4ae-8e7d5222a520) qui va représenter une petite base de données de commandes de produits.

    Nous allons dans un premier temps le prendre en main pour essayer de comprendre à quoi correspond chaque onglet.

    Puis, on se posera quelques questions pour essayer de bien comprendre comment tout ceci fonctionne :).

Lorsque l'on a joué avec Kahoot, le fichier Excel était composé de 4 onglets.
Chaque onglet représentait un et un seul type d'informations/données.
Tous ces onglets avaient des liens avec un ou plusieurs autres : il s'agissaient de données liées les unes aux autres.

Un fichier Excel structuré de cette façon est totalement assimilable à une `base de données relationnelle` (une base SQL en gros) !

- le fichier excel <=> une `base de données` SQL
- un onglet <=> une `table` de notre base
- une ligne d'un onglet <=> un `enregistrement` dans une table

La première colonne des onglets *catalogue*, *clients*, *commandes* correspond à un identifiant unique de chaque ligne

- ce type de colonne (qui identifie de manière certaine un et un seul enregistrement) <=> `clé primaire` en BDD

Certaines colonnes de certains onglets faisaient référence à une ligne d'un autre onglet (c'est ça le lien que l'on a constaté entre tous nos onglets).

- une colonne qui fait référence à une autre colonne <=> une `clé étrangère` en BDD

